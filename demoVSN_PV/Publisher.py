import zmq 
import time
import cv2
from random import randint
from lib.datatransfer.Message_pb2 import *

cameraID = 1

if __name__ == "__main__":
 # if you call this script from the command line (the shell) it will run the 'main' function
 
 # Create a ZMQ_PUB socket and bind publisher to all interfaces
 context = zmq.Context()
 publisher = context.socket(zmq.PUB)
 publisher.bind("tcp://*:%i" % 9000)

 print "OpenCV version:"
 print cv2.__version__

 while True:
    cap = cv2.VideoCapture(0)			#device 0 is video capture device

    prev = time.time()
    while(True):
        ret, frame = cap.read()			#capture images from the camera
                   
        msg = ImageInfo()			#create a imageinfo msg
        msg.cameraID = cameraID
        msg.image = frame.tostring()
        tnow = float(time.time())
        msg.timestamp = tnow #+ (float)(time.time_ns())*1e-9

        print("%f vs %f" % (tnow, msg.timestamp))
                
        rand=randint(0,9)			#with a probability of 30% try to find a contour in the image and send it

        # if rand > 7:
        #     imgray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #     ret, thresh = cv2.threshold(imgray, 127, 255, 0)
        #     im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #
        #     c = max(contours, key = cv2.contourArea)
        #
        #     (x, y, w, h) = cv2.boundingRect(c)
        #
        #     msg.bbX = x
        #     msg.bbY = y
        #     msg.bbWidth = w
        #     msg.bbHeight = h
        #     msg.conf=1.2
        
        binaryMsg = msg.SerializeToString()        		#serialize the message using protocol buffers
        publisher.send_multipart(["Image", binaryMsg])   	#send the message using ZMQ and a certain topic
        
        if(msg.bbHeight!=0):					#if a contour was send, send also a control message
            cmd = AdditionalControl()
            cmd.command = "Tracking at cam %i"%cameraID
            cmd.cameraID = cameraID
            binaryCmd = cmd.SerializeToString()
            publisher.send_multipart(["Command", binaryCmd]) 	#control messages have a different topic

        #time.sleep(0.5)
        now = time.time()
        elapsed = now - prev
        prev = now
        stime = 0.03 - elapsed
        if stime > 0:
            time.sleep(0.03 - elapsed)

        print("send -- " + str(elapsed) + " -- " + str(msg.timestamp) + " -- " + str(time.time()))
