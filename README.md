# Pervasive Computing - Seamless Sensor Switching (PC_SSS)

## Project Description

### Abstract
Technological advances in recent years have led to an enormous increase of smart
devices in use. For human interaction nowadays nearly all devices rely on visual
interaction in form of screens. It is much likely that the amount of visual 
output devices will increase and will seemingly be integrated into our 
surroundings. However state-of-the-art applications do not allow flawless screen
switching while using the same application. We propose with this project an 
Seamless Screen Switching Application using standard VGA cameras for human 
tracking. The aim of our project is to track the human operators motion and 
display the currently used application output on the (part of the) screen in 
sight. In this project we further want to focus on the usage of video streaming 
on the screen in sight.

This project was created as part of the [Pervasive Computing Course](https://nes.aau.at/?page_id=6357) 
at [University of Klagenfurt](https://www.aau.at/), Winter Term 2018.

### Hardware List

- Several visual output devices, such as monitors or tablets.
- Several Raspberry PI’s attached to monitors.
- Intel NUC based, ready to use camera network in the PC lab
- Processing PC

### Software Info

The "MPII" model is too big. We have therefore linked it to Dropbox.
Currently we are using the MPII model.

- [ModelLink](https://www.dropbox.com/s/qqy5e06gtousxwj/pose_iter_160000.caffemodel?dl=0)

Further ```pose_alg_cam.py``` is run on the NUCs, ```srec.py``` is being run
on the Raspberry PIs. ```main.py``` starts the main gateway program on the base station.

#### Prequesites

We used Python 3.5 plus the following installed packages:

- OpenCV
- ZMQ (incl. Protobuf)
- NumPy

```bash
pip3 install --user opencv-python zmq protobuf
```

### License

### Version

Currently Beta 1.0