import zmq
import sys
import cv2
import zlib
import numpy as np
from PIL import Image
from lib.datatransfer.Message_pb2 import *

screen_id = 3


def mprint(msg):
    """
    Own print function, to improve output logs.
    :param msg:
    :return:
    """
    print("[SREC] " + str(msg))
    sys.stdout.flush()
    pass


if __name__ == "__main__":

    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    address = "tcp://192.168.2.169:9002"
    mprint("Connecting to " + address)
    socket.connect(address)
    socket.subscribe("")
    mprint("Connection successfull")

    output = np.zeros((1080, 1920, 3), dtype="uint8")

    # MAIN LOOP
    while True:
        data = socket.recv_multipart(2)

        # Decode Image Data
        if data[0].decode("utf-8") == 'Image':
            mprint("RECEIVED IMAGE")

            # Parse Image Data
            msg = ImageInfo()
            msg.ParseFromString(data[1])

            if msg.cameraID == screen_id:
                # Convert Image Data
                image_data = zlib.decompress(msg.image)

                # Resize Image to RGB channel
                output = np.frombuffer(image_data, dtype=np.uint8).reshape(1080, 1920, 3)
                pass
            else:
                output = np.zeros((1080, 1920, 3), dtype="uint8")
                pass

            cv2.namedWindow("IMG", cv2.WND_PROP_FULLSCREEN)
            cv2.setWindowProperty("IMG", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow("IMG", output)
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break
                pass
            pass
        pass
    pass