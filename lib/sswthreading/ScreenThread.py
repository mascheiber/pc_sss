from lib.sswthreading.SSWThread import *
import mss as mss

import zmq
import time
import zlib
from PIL import Image
from lib.datatransfer.Message_pb2 import *

import cv2
import numpy as np


class ScreenThread(SSWThread):
    """
    Screen Thread class used for screen stream forwarding.
    """

    def __init__(self, tid):
        SSWThread.__init__(self, tid)

        # Set Name
        self._Name = " ST" + str(tid)

        # Set FPS
        self._FPS = 5
        self._TPeriod = float(1/float(self._FPS))
        self._tprint("FPS: %i; Period: %f" % (self._FPS, self._TPeriod))

        # Set Socket
        self._Context = zmq.Context()
        self._Socket = self._Context.socket(zmq.PUB)
        print("here4")
        self._Socket.bind("tcp://*:9002")
        print("here5")

        # Set Private vars
        self._ScreenDisp = 0

        self._tprint("Created Screen Thread with tcp://*:9002")
        pass

    def run(self):
        self._tprint("Running")
        tprev = time.time()
        tnow = tprev
        while self._Running:
            self.screen_shot()

            # if image is shown
            # if cv2.waitKey(25):  # & 0xFF == ord("q"):
            #     cv2.destroyAllWindows()
            #     break
            #     pass
            # pass

            tnow = time.time()
            elapsed = tnow - tprev
            # self._tprint("Elapsed: %f" % elapsed)
            if elapsed < self._TPeriod:
                time.sleep(self._TPeriod - elapsed)
                pass
            tprev = time.time()
        pass

    def screen_shot(self):
        """
        Takes screen shot and sends it via ZMQ socket.
        :return: None
        """
        title = "TEST Imgs"
        monitor = {"top": 0, "left": 0, "width": 1920, "height": 1080}
        with mss.mss() as sct:
            img_raw = sct.grab(monitor)
            img_rgb = self.__pil_frombytes(img_raw)

            # Show image if necessary
            #img = np.asarray(sct.grab(monitor), dtype=np.uint8)
            #cv2.imshow(title, img)

            msg = ImageInfo()
            msg.cameraID = self._ScreenDisp # abuse cam ID for active screen
            msg.image = zlib.compress(img_rgb) #img.tobytes() #img_rgb #.tostring()
            msg.timestamp = float(time.time())

            binaryMsg = msg.SerializeToString()  # serialize the message using protocol buffers
            self._Socket.send_multipart(["Image", binaryMsg])  # send the message using ZMQ and a certain topic
            # self._tprint("Send image.")
            pass
        pass

    def set_disp_screen(self, sc_no):
        """
        Set active screen.
        :param sc_no: ID of active screen.
        :return: None
        """
        if sc_no < 0 | sc_no > 3:
            self._tprint("Screen %i not found." % sc_no)
            pass
        else:
            self._ScreenDisp = sc_no
            self._tprint("Activating Screen %i" % self._ScreenDisp)
            pass
        pass

    def __pil_frombytes(self, img):
        """
        Convert PIL image to bytes
        :param img: cv2 img
        :return: img in byte array
        """
        imag = Image.frombytes('RGB', img.size, img.bgra, 'raw', 'BGRX')
        b, g, r = imag.split()
        return Image.merge("RGB", (r, g, b)).tobytes()

    pass  # class ScreenThread()
