import threading
import time
import sys

class SSWThread(threading.Thread):

    def __init__(self, tid):
        print("[SSWT] Creating Thread T" + str(tid))
        threading.Thread.__init__(self)

        # Construct Thread Values from arguments
        self._TID = tid

        # Other Thread values
        self._Running = False
        self._Name = "T" + str(tid)
        pass

    def run(self):
        while self._Running:
            self._tprint("running ...")
            time.sleep(0.5)
            pass

    def init(self):
        self._tprint("Starting thread.")
        self._Running = True
        pass

    def stop(self):
        self._tprint("Stopping thread.")
        self._Running = False
        pass

    def _tprint(self, msg):
        print("[" + self._Name + "] " + str(msg))
        sys.stdout.flush()
        pass
