from lib.sswthreading.SSWThread import *
import zmq
import time
import os
import math
import numpy as np
import cv2
from PIL import Image
from lib.datatransfer.Message_pb2 import *


class ReceiverThread(SSWThread):
    """
    Simple Receiver Thread for receiving ZMQ images over TCP connection
    """
    # Common Vars for all Classes
    imgWidth = 640      # cam img width
    imgHeight = 480     # cam img height

    def __init__(self, tid, ip, port):
        SSWThread.__init__(self, tid)

        # Set Name
        self._Name = " RT" + str(tid)

        # SC cnt
        self._ScCnt = [0, 0, 0, 0]

        # Construct Thread Values from arguments
        self._IP = ip
        self._Port = port

        # Other Thread values
        self._SaveImg = False
        self._Context = zmq.Context()
        self._Socket = self._Context.socket(zmq.SUB)
        self._SaveDir = "calib"
        pass

    def run(self):
        while self._Running:

            # Receive Data
            data = self._Socket.recv_multipart(2)

            # Decode Image Data
            if data[0].decode("utf-8") == 'Image':
                self._tprint("RECEIVED IMAGE")

                # Parse Image Data
                msg = ImageInfo()
                msg.ParseFromString(data[1])

                # Convert Image Data
                camera_id = msg.cameraID
                image_data = msg.image
                image1 = Image.frombytes('RGB', (640, 480), image_data)
                img_cv = np.array(image1)
                # b, g, r = image1.split()
                # image = Image.merge("RGB", (r, g, b))

                #cv2.imshow('Output-Img', img_cv)
                #cv2.waitKey(0)


                #TODO(martin): Process image here
                #self._screen_alg(img_cv, camera_id)
                pass
            # Decode Screen Msg Data
            elif data[0].decode("utf-8") == 'ScreenMsg':
                self._tprint("RECEIVED SCREENMSG")

                # Parse Data
                msg = ImageInfo()
                msg.ParseFromString(data[1])

                # Convert Screen Data
                camera_id = msg.cameraID
                screen_data = msg.image
                conv_data = [ord(x) for x in screen_data]
                for x in conv_data:
                    self._inc_sc_cnt(x-1)
                self._tprint("Received data: ")
                self._tprint(conv_data)

            self._tprint("Done Receiving\n")
            # time.sleep(0.5)
            pass

    def init(self):
        """
        Initialize the Thread
        :return: None
        """
        self._tprint("Starting receiving.")

        # Connect to TCP socket
        address = "tcp://" + str(self._IP) + ":" + str(self._Port)
        self._tprint("Connecting to " + address)
        self._Socket.connect(address)
        self._Socket.subscribe("")
        self._tprint("Connection success.")

        # Set Running to True
        self._Running = True
        pass

    def set_save_dir(self, directory):
        """
        Changes the save directory for camera images.
        :param directory: directory to set path to
        :return: None
        """
        self._SaveDir = directory
        pass

    def get_sc_cnt(self):
        """
        Get full screen count from this connection.
        :return: list of screen count
        """
        return self._ScCnt

    def reset_sc_cnt(self):
        """
        Reset screen count from this connection.
        :return: None
        """
        self._ScCnt = [0, 0, 0, 0]
        pass

    def _inc_sc_cnt(self, id):
        """
        Increase screen count for id.
        :param id: Screen ID to increase count for
        :return: None
        """
        self._ScCnt[id] = self._ScCnt[id] + 1
        pass

    def __check_save_dir(self):
        """
        Checks if the save directory exists.
        :return: None
        """
        if not os.path.exists(self._SaveDir):
            os.mkdir(self._SaveDir)
            self._tprint("Created directory " + str(self._SaveDir))
            pass
        pass

    pass
