from lib.sswthreading.ReceiverThread import *
from lib.sswthreading.ScreenThread import *


def mprint(msg):
    print("[MAIN] " + str(msg))
    sys.stdout.flush()
    pass


if __name__ == "__main__":

    threads = []
    directory = os.getcwd() + "/data/calib05"

    # Create Threads
    try:
        # Create Screen Forwarding Thread
        mprint("Trying to create Screen Thread")
        thread = ScreenThread(2)
        mprint("Created thread")
        threads += [thread]

        # Create Cam Receiver Threads
        for x in range(0, 5):
            mprint("Trying to create Thread")
            ip = "192.168.1.15" + str(x+1)
            thread = ReceiverThread(x, ip, 9000)
            threads += [thread]
            pass

    except Exception as e:
        mprint("Error creating threads.")
        mprint(str(e))
        pass

    try:
        mprint("Initializing threads.")
        for t in threads:
            t.init()
            pass
        pass
    except Exception as e:
        mprint("Could not initialize threads")
        mprint(str(e))
        pass

    try:
        mprint("Starting threads.")
        for t in threads:
            t.start()
            pass
        pass
    except Exception as e:
        mprint("Could not start threads")
        mprint(str(e))
        pass

    # MAIN LOOP
    #for i in range(60):
    i = 0
    while True:
        # if i % 10 == 0:
        #     threads[0].set_disp_screen(int(i / 10) % 4)
        #     pass
        # i = i + 1
        # Get all screen counts
        cnt = [0, 0, 0, 0]
        for i in range(1, len(threads)):
            tcnt = threads[i].get_sc_cnt()
            mprint(tcnt)
            for j in range(0,4):
                cnt[j] = cnt[j] + tcnt[j]
                pass
            threads[i].reset_sc_cnt()
            pass
        mprint(cnt)

        # Get Max
        max = -1
        maxid = []
        k = 0
        for c in cnt:
            if c > max:
                maxid = [k]
                max = c
                pass
            elif c == max:
                maxid.append(k)
                pass
            k = k + 1
            pass

        mprint("MaxScreen:")
        mprint(maxid)
        mprint("at: %i" % max)

        # Activate Max Screen
        for i in maxid:
            threads[0].set_disp_screen(i)
            pass

        time.sleep(5)
        pass

    # For timed program
    for t in threads:
        t.stop()
        t.join()
        pass
