import zmq
import time
import cv2
import base64
import numpy as np
import math
import os
from random import randint
from Message_pb2 import *

cameraID = 3

context = ""
publisher = ""


def send_img(frame):
    print(type(frame))
    msg = ImageInfo()  # create a imageinfo msg
    msg.cameraID = cameraID
    msg.image = frame.tostring()
    msg.timestamp = float(time.time())  # + (float)(time.time_ns())*1e-9

    binaryMsg = msg.SerializeToString()  # serialize the message using protocol buffers
    print(type(binaryMsg))
    publisher.send_multipart(["Image".encode("utf8"), binaryMsg])  # send the message using ZMQ and a certain topic
    pass


def send_scmsg(values):
    msg = ImageInfo()
    msg.cameraID = cameraID
    msg.image = bytes(values)
    msg.timestamp = float(time.time())  # + (float)(time.time_ns())*1e-9

    binaryMsg = msg.SerializeToString()
    publisher.send_multipart(["ScreenMsg".encode("utf-8"), binaryMsg])
    pass


def screen_alg(photo, camid):
    print("Deriving dir for %i" % camid)
    print("Having type: " + str(type(photo)))

    # loading pre-trained models "MPII model"
    # --------------------------

    # .protoxt specifies the architecture of the neural network
    protoFile = "pose_deploy_linevec_faster_4_stages.prototxt"
    # .caffemodel file which stores the weights of the trained model
    weightsFile = "pose_iter_160000.caffemodel"
    nPoints = 15
    POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 14], [14, 8], [8, 9], [9, 10],
                  [14, 11], [11, 12], [12, 13]]

    # loading photo

    frame = photo
    # frame = cv2.imread(photo)
    # frameCopy = np.copy(frame)

    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]
    threshold = 0.1

    print("W: %i, H: %i" % (frameWidth, frameHeight))

    net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)

    t = time.time()

    # input image dimensions for the network
    inWidth = 368
    inHeight = 368
    inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight),
                                    (0, 0, 0), swapRB=False, crop=False)

    net.setInput(inpBlob)

    print("asdfasdfasdf")
    output = net.forward()
    print("time taken by network : {:.3f}".format(time.time() - t))

    H = output.shape[2]
    W = output.shape[3]

    # Empty list to store the detected keypoints
    points = []

    for i in range(nPoints):
        # confidence map of corresponding body's part.
        probMap = output[0, i, :, :]

        # Find global maxima of the probMap.
        minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

        # Scale the point to fit on the original image
        x = (frameWidth * point[0]) / W
        y = (frameHeight * point[1]) / H

        if prob > threshold:
            # Add the point to the list if the probability is greater than the threshold
            points.append((int(x), int(y)))
        else:
            points.append(None)

    # Draw Skeleton
    for pair in POSE_PAIRS:
        partA = pair[0]
        partB = pair[1]

        if points[partA] and points[partB]:
            cv2.line(frame, points[partA], points[partB], (0, 255, 255), 2)
            cv2.circle(frame, points[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)

    image_points = np.array(points)

    # if (None in image_points):
    # print(None)
    # return None

    # 3D model of body
    model_points = np.array([

        (50.0, 0.0, 0.0),  # Head
        (0.0, 0.0, 0.0),  # Neck

        (0.0, -25.0, 0.0),  # Right shoulder
        (-60.0, -25.0, -10.0),  # Right elbow
        (-130.0, -25.0, 0.0),  # Right wrist

        (0.0, 25.0, 0.0),  # Left shoulder
        (-60.0, 25.0, -10.0),  # Left elbow
        (-130.0, 25.0, 0.0),  # Left wrist

        (-130.0, -20.0, 0.0),  # Right hip
        (-180.0, -20.0, 5.0),  # Right knee
        (-220.0, -20.0, 0.0),  # Right ankle

        (-130.0, 20.0, 0.0),  # Left hip
        (-180.0, 20.0, 5.0),  # Left knee
        (-220.0, 20.0, 0.0),  # Left ankle

        (-60.0, 0.0, 0.0)  # Chest

    ])

    newimp = []
    newmp = []
    if (None in image_points):
        for i in range(len(image_points)):
            if (image_points[i] != None):
                newimp.append(image_points[i])
                newmp.append([model_points[i][0], model_points[i][1], model_points[i][2]])

        model_points = (np.array(newmp)).astype(float)
        image_points = (np.array(newimp)).astype(float)
    else:
        model_points = (np.array(model_points)).astype(float)
        image_points = (np.array(image_points)).astype(float)

    if (len(image_points) <= 10):
        print("None")
        return None

    # approximate the focal length
    size = frame.shape
    focal_length = size[1]
    center = (size[1] / 2, size[0] / 2)

    # camera matrix
    camera_matrix = np.array(
        [[focal_length, 0, center[0]],
         [0, focal_length, center[1]],
         [0, 0, 1]], dtype="double"
    )

    print("Camera Matrix :\n {0}".format(camera_matrix))

    dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion

    (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, image_points, camera_matrix,
                                                                  dist_coeffs, flags=cv2.SOLVEPNP_ITERATIVE)

    # drawing lines

    # angle of sight
    angle = 10
    angle = math.radians(angle)
    angle = 1 / math.tan(angle)

    lenline = 500
    rangeline = lenline / (angle / 2)

    axis = np.float32([[0, -rangeline, lenline], [0, rangeline, lenline], [0, 0, lenline]])

    (imgtp, jacobian) = cv2.projectPoints(axis, rotation_vector, translation_vector, camera_matrix, dist_coeffs)

    neck_point = tuple(image_points[1].astype(int))
    left_line = tuple(imgtp[0].ravel())
    right_line = tuple(imgtp[1].ravel())
    middle_line = tuple(imgtp[2].ravel())
    print(right_line)
    print(left_line)

    img2 = cv2.line(frame, neck_point, left_line, (0, 0, 255), 2)
    img2 = cv2.line(frame, neck_point, right_line, (0, 0, 255), 2)
    img2 = cv2.line(frame, neck_point, middle_line, (0, 0, 255), 2)

    fslope = (-left_line[1] - -neck_point[1]) / (left_line[0] - neck_point[0])
    sslope = (-right_line[1] - -neck_point[1]) / (right_line[0] - neck_point[0])
    mslope = (-middle_line[1] - -neck_point[1]) / (middle_line[0] - neck_point[0])

    sc1 = []
    sc2 = []
    sc3 = []
    sc4 = []

    if (camid == 1):
        sc1 = [(95, 480), (220, 480)]
        sc2 = [(0, 30), (0, 150)]
        sc3 = [(640, 150), (640, 265)]
        sc4 = [(640, 290), (640, 390)]

    if (camid == 2):
        sc1 = [(640, 150), (640, 380)]
        sc2 = [(0, 300), (220, 480)]
        sc3 = [(60, 0), (110, 0)]
        sc4 = [(330, 0), (410, 0)]
        # img2 = cv2.line(frame,(0,65),(0,150),(0,255,0), 2) #screen 2
        # img2 = cv2.line(frame,(640,165),(640,260),(0,255,0), 2)# screen 3
        # img2 = cv2.line(frame,(640,290),(640,375),(255,0,0), 2)# screen 4

    if (camid == 3):
        sc1 = [(640, 200), (640, 360)]
        sc2 = [(130, 480), (310, 480)]
        sc3 = [(0, 75), (0, 180)]
        sc4 = [(185, 35), (310, 130)]

    if (camid == 4):
        sc1 = [(360, 75), (430, 145)]
        sc2 = [(640, 80), (640, 230)]
        sc3 = [(230, 480), (400, 480)]
        sc4 = [(0, 70), (0, 220)]

    if (camid == 5):
        sc1 = [(0, 280), (0, 460)]
        sc2 = [(0, 65), (0, 210)]
        sc3 = [(520, 180), (625, 305)]
        sc4 = [(410, 480), (620, 480)]

    # print(sc1)
    img2 = cv2.line(frame, sc1[0], sc1[1], (0, 255, 0), 2)  # screen 1
    img2 = cv2.line(frame, sc2[0], sc2[1], (0, 255, 0), 2)  # screen 2
    img2 = cv2.line(frame, sc3[0], sc3[1], (0, 255, 0), 2)  # screen 3
    img2 = cv2.line(frame, sc4[0], sc4[1], (0, 255, 0), 2)  # screen 4

    screen_value = []
    if (screen_bool("screen1", fslope, sslope, mslope, sc1[0], sc1[1], neck_point, right_line, left_line,
                    middle_line)) == "YES":
        screen_value.append(1)

    if (screen_bool("screen2", fslope, sslope, mslope, sc2[0], sc2[1], neck_point, right_line, left_line,
                    middle_line)) == "YES":
        screen_value.append(2)
    if (screen_bool("screen3", fslope, sslope, mslope, sc3[0], sc3[1], neck_point, right_line, left_line,
                    middle_line)) == "YES":
        screen_value.append(3)
    if (screen_bool("screen4", fslope, sslope, mslope, sc4[0], sc4[1], neck_point, right_line, left_line,
                    middle_line)) == "YES":
        screen_value.append(4)

    # cv2.imshow('Output-Skeleton', frame)

    # cv2.imwrite('Output-Skeleton.jpg', frame)

    print("Total time taken : {:.3f}".format(time.time() - t))

    print(screen_value)
    send_scmsg(screen_value)
    # cv2.waitKey(0)


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])  # Typo was here

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def screen_bool(screen, fslope, sslope, mslope, left, right, neck_point, rightline, leftline, middleline):
    y1_1 = fslope * (left[0] - neck_point[0]) + (-neck_point[1])
    y1_2 = fslope * (right[0] - neck_point[0]) + (-neck_point[1])
    y2_1 = sslope * (left[0] - neck_point[0]) + (-neck_point[1])
    y2_2 = sslope * (right[0] - neck_point[0]) + (-neck_point[1])
    y3_1 = mslope * (left[0] - neck_point[0]) + (-neck_point[1])
    y3_2 = mslope * (right[0] - neck_point[0]) + (-neck_point[1])
    '''
    print(leftline)
    print(rightline)
    print(neck_point)
    print(right)
    print(left)
    '''
    a = line_intersection([neck_point, leftline], [left, right])
    b = line_intersection([neck_point, rightline], [left, right])

    rl_x = math.copysign(1, (neck_point[0] - rightline[0]))
    r_x = math.copysign(1, (neck_point[0] - b[0]))
    sudo
    apt - get
    install
    libjpeg - dev
    libtiff5 - dev
    libjasper - dev
    libpng12 - dev

    ll_x = math.copysign(1, (neck_point[0] - leftline[0]))
    l_x = math.copysign(1, (neck_point[0] - a[0]))

    rl_y = math.copysign(1, (-neck_point[1] + rightline[1]))
    r_y = math.copysign(1, (-neck_point[1] + b[1]))

    ll_y = math.copysign(1, (-neck_point[1] + leftline[1]))
    l_y = math.copysign(1, (-neck_point[1] + a[1]))

    ml_x = math.copysign(1, (neck_point[0] - middleline[0]))
    ml_y = math.copysign(1, (-neck_point[1] + middleline[1]))

    answer = "NO"
    if ((y3_1 <= -left[1] and y3_2 >= -right[1]) and ((ml_x == r_x) and (ml_y == r_y))):
        answer = "YES"
    elif ((y1_1 <= -left[1] and y1_2 >= -right[1]) and ((ll_x == r_x) and (ll_y == r_y))):
        answer = "YES"
    elif ((y2_1 <= -left[1] and y2_2 >= -right[1]) and ((rl_x == r_x) and (rl_y == r_y))):
        answer = "YES"

    return answer


if __name__ == "__main__":
    # if you call this script from the command line (the shell) it will run the 'main' function

    # Create a ZMQ_PUB socket and bind publisher to all interfaces
    context = zmq.Context()
    publisher = context.socket(zmq.PUB)
    publisher.bind("tcp://*:%i" % 9000)

    print
    "OpenCV version:"
    print
    cv2.__version__

    while True:
        cap = cv2.VideoCapture(0)  # device 0 is video capture device

        prev = time.time()
        while (True):
            ret, frame = cap.read()  # capture images from the camera
            print(type(frame))

            # msg = ImageInfo()  # create a imageinfo msg
            # msg.cameraID = cameraID
            # msg.image = frame.tostring()
            # tnow = float(time.time())
            # msg.timestamp = tnow  # + (float)(time.time_ns())*1e-9
            #
            # print("%f vs %f" % (tnow, msg.timestamp))

            # Parse Image Data
            # msg = ImageInfo()
            # msg.ParseFromString(data[1])

            # Convert Image Data
            # camera_id = msg.cameraID
            # image_data = msg.image
            # image1 = Image.frombytes('RGB', (640, 480), image_data)
            # img_cv = np.array(image1)
            # b, g, r = image1.split()
            # image = Image.merge("RGB", (r, g, b))

            # TODO(martin): Process image here
            screen_alg(frame, cameraID)

            # binaryMsg = msg.SerializeToString()  # serialize the message using protocol buffers
            # publisher.send_multipart(["Image", binaryMsg])  # send the message using ZMQ and a certain topic
            #
            # if (msg.bbHeight != 0):  # if a contour was send, send also a control message
            #     cmd = AdditionalControl()
            #     cmd.command = "Tracking at cam %i" % cameraID
            #     cmd.cameraID = cameraID
            #     binaryCmd = cmd.SerializeToString()
            #     publisher.send_multipart(["Command", binaryCmd])  # control messages have a different topic

            # time.sleep(0.5)
            now = time.time()
            elapsed = now - prev
            prev = now
            stime = 0.5 - elapsed
            if stime > 0:
                time.sleep(0.5 - elapsed)

            # print("send -- " + str(elapsed) + " -- " + str(msg.timestamp) + " -- " + str(time.time()))
            pass
